<?php

namespace Drupal\tojio_tweaks\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class Settings
 *
 * @package Drupal\tojio_tweaks\Form
 */
class Settings extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'tojio_tweaks_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'tojio_tweaks.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('tojio_tweaks.settings');

    $form['tojio_tweaks_settings'] = [
      '#tree' => TRUE,
      '#type' => 'fieldset',
      '#title' => $this->t('Remove security updates messages'),
    ];

    $form['tojio_tweaks_settings']['remove_error_update_messages_enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable removing of security update ERROR messages.'),
      '#default_value' => $config->get('tojio_tweaks_settings.remove_error_update_messages_enabled'),
    ];

    $form['tojio_tweaks_settings']['remove_warning_update_messages_enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable removing of security update WARNING messages.'),
      '#default_value' => $config->get('tojio_tweaks_settings.remove_warning_update_messages_enabled'),
    ];

    $user_role_names = user_role_names();
    $config_user_role_names = $config->get('tojio_tweaks_settings.remove_update_messages_roles');
    $user_role_names_default = $config_user_role_names === NULL ? $user_role_names : $config_user_role_names;

    $form['tojio_tweaks_settings']['remove_update_messages_roles'] = [
      '#type'          => 'checkboxes',
      '#title'         => $this->t('Remove security update messages for selected roles:'),
      '#default_value' => $user_role_names_default,
      '#options'       => user_role_names(),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('tojio_tweaks.settings');
    $config->set('tojio_tweaks_settings', $form_state->getValue('tojio_tweaks_settings'));
    $config->save();
    parent::submitForm($form, $form_state);
  }

}
